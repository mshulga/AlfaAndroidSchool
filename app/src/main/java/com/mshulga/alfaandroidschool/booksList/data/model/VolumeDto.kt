package com.mshulga.alfaandroidschool.booksList.data.model

import kotlinx.serialization.Serializable

@Serializable
data class VolumeDtoWrapper(
    val totalItems: Int,
    val items: List<VolumeDto>
)

@Serializable
data class VolumeDto(
    val id: String,
    val volumeInfo: VolumeInfoDto
)

@Serializable
data class VolumeInfoDto(
    val title: String,
    val authors: List<String> = arrayListOf(),
    val description: String,
    val imageLinks: VolumeInfoImage
)

@Serializable
data class VolumeInfoImage(val thumbnail: String)