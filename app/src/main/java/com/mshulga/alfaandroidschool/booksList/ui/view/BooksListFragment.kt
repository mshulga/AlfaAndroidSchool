package com.mshulga.alfaandroidschool.booksList.ui.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.mshulga.alfaandroidschool.R
import com.mshulga.alfaandroidschool.booksList.data.mapper.BooksToEntityMapper
import com.mshulga.alfaandroidschool.booksList.data.repository.BooksRepositoryImpl
import com.mshulga.alfaandroidschool.booksList.domain.model.VolumeEntity
import com.mshulga.alfaandroidschool.booksList.ui.vm.BooksListViewModel
import com.mshulga.alfaandroidschool.booksList.ui.vm.BooksListViewModelProviderFactory
import com.mshulga.alfaandroidschool.network.provideRetrofit
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import retrofit2.create

class BooksListFragment : Fragment(R.layout.fmt_books_list) {

    private val viewModel: BooksListViewModel by viewModels {
        BooksListViewModelProviderFactory(
            BooksRepositoryImpl(
                apiKey = "AIzaSyArqSnozA9aCi5Hud_qKXLkpUILq0vC9tQ",
                toEntityMapper = BooksToEntityMapper(),
                api = provideRetrofit().create()
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.volumes.onEach(::onVolumesReceived).launchIn(lifecycleScope)
    }

    private fun onVolumesReceived(volumes: List<VolumeEntity>) {
    }
}