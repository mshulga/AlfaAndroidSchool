package com.mshulga.alfaandroidschool.booksList.ui.vm

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.mshulga.alfaandroidschool.booksList.domain.BooksRepository
import com.mshulga.alfaandroidschool.booksList.domain.model.VolumeEntity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

class BooksListViewModel(private val repository: BooksRepository) : ViewModel() {

    private val mutableVolumes: MutableStateFlow<List<VolumeEntity>> = MutableStateFlow(arrayListOf())
    val volumes: StateFlow<List<VolumeEntity>> = mutableVolumes.asStateFlow()

    init {
        loadVolumes()
    }

    private fun loadVolumes() {
        viewModelScope.launch {
            mutableVolumes.update { repository.getVolumes(DEFAULT_VOLUMES_QUERY) }
        }
    }

    companion object {
        private const val DEFAULT_VOLUMES_QUERY = "Android"
    }
}

@Suppress("UNCHECKED_CAST")
class BooksListViewModelProviderFactory(private val repository: BooksRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        assert(modelClass == BooksListViewModel::class.java)
        return BooksListViewModel(repository) as T
    }
}