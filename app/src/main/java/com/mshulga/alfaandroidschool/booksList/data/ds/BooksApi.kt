package com.mshulga.alfaandroidschool.booksList.data.ds

import com.mshulga.alfaandroidschool.booksList.data.model.VolumeDtoWrapper
import retrofit2.http.GET
import retrofit2.http.Query

interface BooksApi {

    @GET("/books/v1/volumes")
    suspend fun getVolumes(@Query("q") query: String, @Query("key") apiKey: String): VolumeDtoWrapper
}