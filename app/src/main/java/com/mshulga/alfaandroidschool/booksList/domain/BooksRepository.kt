package com.mshulga.alfaandroidschool.booksList.domain

import com.mshulga.alfaandroidschool.booksList.domain.model.VolumeEntity

interface BooksRepository {
    suspend fun getVolumes(query: String): List<VolumeEntity>
}