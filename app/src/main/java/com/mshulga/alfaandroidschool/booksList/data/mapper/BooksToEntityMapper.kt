package com.mshulga.alfaandroidschool.booksList.data.mapper

import com.mshulga.alfaandroidschool.booksList.data.model.VolumeDto
import com.mshulga.alfaandroidschool.booksList.data.model.VolumeDtoWrapper
import com.mshulga.alfaandroidschool.booksList.domain.model.VolumeEntity

class BooksToEntityMapper {

    fun mapVolumeWrapper(dto: VolumeDtoWrapper): List<VolumeEntity> =
        dto.items.map(::mapVolume)

    fun mapVolume(dto: VolumeDto): VolumeEntity =
        with(dto) {
            VolumeEntity(
                id = id,
                title = volumeInfo.title,
                authors = volumeInfo.authors,
                description = volumeInfo.description,
                thumbnail = volumeInfo.imageLinks.thumbnail
            )
        }
}