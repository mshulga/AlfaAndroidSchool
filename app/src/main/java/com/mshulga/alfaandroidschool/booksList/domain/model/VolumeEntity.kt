package com.mshulga.alfaandroidschool.booksList.domain.model

data class VolumeEntity(
    val id: String,
    val title: String,
    val authors: List<String>,
    val description: String,
    val thumbnail: String
)