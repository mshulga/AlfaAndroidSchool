package com.mshulga.alfaandroidschool.booksList.data.repository

import com.mshulga.alfaandroidschool.booksList.data.ds.BooksApi
import com.mshulga.alfaandroidschool.booksList.data.mapper.BooksToEntityMapper
import com.mshulga.alfaandroidschool.booksList.domain.BooksRepository
import com.mshulga.alfaandroidschool.booksList.domain.model.VolumeEntity

class BooksRepositoryImpl(
    private val api: BooksApi,
    private val apiKey: String,
    private val toEntityMapper: BooksToEntityMapper,
) : BooksRepository {
    override suspend fun getVolumes(query: String): List<VolumeEntity> {
        val dto = api.getVolumes(query = query, apiKey = apiKey)
        return toEntityMapper.mapVolumeWrapper(dto)
    }

}